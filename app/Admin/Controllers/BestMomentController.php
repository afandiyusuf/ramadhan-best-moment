<?php

namespace App\Admin\Controllers;

use App\BestMoment;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Http\Request; 
use App\VideoJob;
use App\User;

class BestMomentController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\BestMoment';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new BestMoment());
        $grid->disableCreateButton();
        $grid->column('id', __('Id'));
        $grid->column('user_id', __('User id'))->sortable();
        $grid->column('user.name','Name');
        $grid->column('user.email','Email');
        $grid->column('order', __('Order'))->sortable();;
        $grid->column('image_url','Image')->display(function($image_url){
            return '<a href="/storage/'.$image_url.'" target="_blank"><img height="40px" src="/storage/'.$image_url.'" alt="avatar invalid" /></a>';
        });
        $grid->column('is_valid')->display(function($is_valid){

            if($is_valid == 1)
            {
                $status = 'Accepted';
                $class = 'label label-success';
            }else if($is_valid == 2){
                $status = 'Not Accepted';
                $class = 'label label-warning';
            }else{
                $status = 'Declined';
                $class = 'label label-danger';
            }
            return '<span class="'.$class.'">'.$status.'</span>';
        })->filter([
            0=> 'declined',
            1=> 'accepted',
            2=> 'not accepted'
        ]);

        $grid->column('status', __('Is valid'))->display(function(){

            if($this->is_valid == 1)
            {
                $string = 'Batalkan Gambar';
                $class = 'btn btn-warning';
                $url = '/admin/best-moments/decline/'.$this->id;
                $add = '';
            }else if($this->is_valid == 0){
                $string = 'Terima Kembali';
                $class = 'btn btn-success';
                $url = '/admin/best-moments/accept/'.$this->id;
                $add = '';
                
            }
            if($this->is_valid == 2)
            {
                $string = 'Terima Gambar';
                $class = 'btn btn-success';
                $url = '/admin/best-moments/accept/'.$this->id;
                $class2 = 'btn btn-danger';
                $url2 = '/admin/best-moments/decline/'.$this->id;
                $add = '<a style="margin-right:5px;" class="'.$class2.'" href="'.$url2.'">Tolak Gambar</a>';
            }

            
            
            return $add.'<a class="'.$class.'" href="'.$url.'">'.$string.'</a>';
        });
        $grid->column('created_at', __('Created at'))->sortable();
        $grid->model()->where('is_active',1);
        // $grid->column('updated_at', __('Updated at'));
        // The filter($callback) method is used to set up a simple search box for the table
        $grid->filter(function ($filter) {

            // Sets the range query for the created_at field
            $filter->disableIdFilter();
            $filter->like('user.name', 'Name');
            $filter->like('user.email', 'Email');
        });

       
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(BestMoment::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('user_id', __('User id'));
        $show->field('order', __('Order'));
        $show->field('image_url', __('Image url'));
        $show->field('is_active', __('Is active'));
        $show->field('is_valid', __('Is valid'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new BestMoment());

        $form->number('user_id', __('User id'));
        $form->switch('order', __('Order'));
        $form->text('image_url', __('Image url'));
        $form->switch('is_active', __('Is active'));
        $form->switch('is_valid', __('Is valid'));

        return $form;
    }

    public function accept(Request $request, BestMoment $bestMoment)
    {
        $bestMoment->is_valid = 1;
        $bestMoment->save();
        //check that user is already had 10 items
        $total = BestMoment::where('user_id',$bestMoment->user_id)->where('is_active',1)->count();
        //trigger create job for video
        if($total == 10)
        {
            $user = User::find($bestMoment->user_id);
            $user->completed = 1;
            $user->save();
            //create job
            //check that already have video job
            $videoJob = VideoJob::where('user_id',$user->id)->first();

            if(!$videoJob)
            {
                $videoJob = new VideoJob();
                $videoJob->user_id = $user->id;
            }
            
            $videoJob->status = 0;
            $videoJob->save();
        }
        return redirect()->back();
    }
    public function decline(Request $request, BestMoment $bestMoment)
    {
        $bestMoment->is_valid = 0;
        $bestMoment->save();
        return redirect()->back();
    }
}

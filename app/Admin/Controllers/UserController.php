<?php

namespace App\Admin\Controllers;

use App\User;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class UserController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\User';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new User());
        $grid->disableCreateButton();
        $grid->column('id', __('Id'));
        $grid->column('name', __('Name'))->sortable();
        $grid->column('email', __('Email'))->sortable();
        $grid->column('gender', __('Gender'))->display(function($gend){
            return $gend == 1?'Laki-laki':'Perempuan';
        })->sortable();
        $grid->column('avatar_url')->display(function($avatar_url){
            if($avatar_url == null)
            {
                return "user tidak punya avatar";
            }
            return '<a href="'.$avatar_url.'" target="_blank"><img height="40px" src="'.$avatar_url.'" alt="avatar invalid" /></a>';
        });
        $grid->column('phone_number', __('Phone number'))->sortable();
        $grid->column('blocked','status')->display(function($blocked){
            return ($blocked )? "<span class='label label-danger'>blocked</span>" : "<span class='label label-success'>active</span>";
        })->sortable()->filter(
            [
                0 => 'Active',
                1 => 'Blocked'
            ]);
        $grid->column('Block User','Status User')->display(function(){
            if($this->blocked == 1)
            {
                return '<a class="btn-xs btn-primary" href="/admin/user/toggle-block/'.$this->id.'">Unblock User</a>';
            }else{
                return '<a class="btn-xs btn-danger" href="/admin/user/toggle-block/'.$this->id.'">Block User</a>';
            }
        });
        $grid->column('profile','halaman profile')->display(function(){
            return '<a href="'.route('feeds',['user'=>$this->id]).'" target="_blank">Profile </a>';
        });
        $grid->quickSearch('completed');

        $grid->filter(function ($filter) {

            // Sets the range query for the created_at field
            $filter->like('name', 'Name');
            $filter->like('email', 'Email');
            $filter->like('phone_number', 'Phone Number');
        });
       

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(User::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name', __('Name'));
        $show->field('email', __('Email'));
        $show->field('email_verified_at', __('Email verified at'));
        $show->field('password', __('Password'));
        $show->field('remember_token', __('Remember token'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('gender', __('Gender'));
        $show->field('avatar_url', __('Avatar url'));
        $show->field('phone_number', __('Phone number'));
        $show->field('user_type', __('User type'));
        $show->field('blocked', __('Blocked'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new User());

        $form->text('name', __('Name'));
        $form->email('email', __('Email'));
        $form->datetime('email_verified_at', __('Email verified at'))->default(date('Y-m-d H:i:s'));
        $form->password('password', __('Password'));
        $form->text('remember_token', __('Remember token'));
        $form->switch('gender', __('Gender'));
        $form->text('avatar_url', __('Avatar url'));
        $form->text('phone_number', __('Phone number'));
        $form->switch('user_type', __('User type'))->default(1);
        $form->switch('blocked', __('Blocked'));
        $form->saving(function (Form $form) {
            if($form->password == null || $form->password == '')
            {
                $form->password = $form->model()->password; 
            }else{
                $form->password = bcrypt($form->password);
            }

        });

        return $form;
    }

    public function toggleBlock( User $user)
    {
        $user->blocked = !$user->blocked;
        $user->save();
        return redirect()->back();
    }
}

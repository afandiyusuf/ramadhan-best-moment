<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\Dashboard;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use App\User;
use App\BestMoment;
use Carbon\Carbon;

class HomeController extends Controller
{
    public function index(Content $content)
    {
        // return $content->row('hello world');
        // return $content
        //     ->title('Dashboard')
        //     ->description('Description...')
        //     ->row(Dashboard::title())
        //     ->row(function (Row $row) {

        //         $row->column(4, function (Column $column) {
        //             $column->append(Dashboard::environment());
        //         });

        //         $row->column(4, function (Column $column) {
        //             $column->append(Dashboard::extensions());
        //         });

        //         $row->column(4, function (Column $column) {
        //             $column->append(Dashboard::dependencies());
        //         });
        //     });
        $submited_image = BestMoment::count();
        $user_total = User::count();
        $need_validate_image = BestMoment::where('is_valid',2)->where('is_active',1)->count();
        $submited_image_this_day = BestMoment::whereDate('created_at',Carbon::today())->count();
        $validated_image = BestMoment::where('is_valid',1)->count();
        $rejected_image = BestMoment::where('is_valid',0)->count();
        if($submited_image != 0){
            $need_validate_image_percentage = $need_validate_image/$submited_image * 100;
            $validated_image_percentage = $validated_image/$submited_image * 100;
            $rejected_image_percentage = $rejected_image/$submited_image*100;
        }else{
            $need_validate_image_percentage = 0;
            $validated_image_percentage = 0;
            $rejected_image_percentage = 0;
        }   
        return $content->view('admin.dashboard', compact(['user_total','need_validate_image','need_validate_image_percentage','submited_image','submited_image_this_day','validated_image','validated_image_percentage','rejected_image','rejected_image_percentage']));
    }
}

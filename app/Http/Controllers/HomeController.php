<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BestMoment;
use App\Order;
use App\VideoJob;
use Auth;
use App;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //return allbesst moment by user that activated
        $canUpload = true;
        $reason = "";
        $user = Auth::user();
        $bestMoments = BestMoment::where('user_id',$user->id)
        ->where('is_active',1)->orderBy('order','ASC')
        ->get();

        $userOrder = [false,false,false,false,false,false,false,false,false,false];
        foreach($bestMoments as $b)
        {
            $userOrder[$b->order-1] = $b;
        }
        $currentOrder = 1;
        for($i =0; $i<count($userOrder); $i++)
        {
            if($userOrder[$i] == false)
            {
                $currentOrder = $i+1;
                break;
            }else if($userOrder[$i]){
                if($userOrder[$i]->is_valid == 2){
                    $currentOrder = $i+1;
                    break;
                }
            }
        }

        // //check all order


        // //get current order
        // $currentOrder = $bestMoment = BestMoment::where('user_id',$user->id)->where('is_valid',1)->orderBy('order','DESC')->first();
        // if($currentOrder == null)
        // {
        //     $currentOrder = 1;
        // }else{
        //     $currentOrder = $currentOrder->order+1;
        // }
        
        //get orderData
        $order = Order::where('order',$currentOrder)->first();


        foreach ($bestMoments as $key => $value) {
            if($value->is_valid != 1)
            {
                // $bestMoments[$key]['image_url'] = null;
            }
        }

        $totalPendingImage = BestMoment::where('user_id',$user->id)
            ->where('is_valid',2)
            ->where('is_active',1)
            ->count();

        if($totalPendingImage != 0)
        {
            $canUpload = false;
            $reason = __("pending image");
        }

        $uploadedToday = BestMoment::where('user_id',$user->id)
            ->whereDate('created_at', '=', date('Y-m-d'))->count();
        if($uploadedToday != 0)
        {
            $canUpload = false;
            $reason = __("limit upload");
        }
        $self = Auth::user();
        
        // if(App::environment(['local','staging']) || $self->id == 1  || $self->id == 5)
        // {
            $canUpload = true;
        // }

        $allOrder = Order::orderBy('order','ASC')->get();
        
        if($self->completed == 1)
        {
            $videoJobStatus = VideoJob::where('user_id',$self->id)->first()->status;
        }else{
            $videoJobStatus = 0;
        }
        //check videojob

        return view('upload',compact(['bestMoments','currentOrder','order','canUpload','userOrder','allOrder','reason','self','videoJobStatus']));
    }
}

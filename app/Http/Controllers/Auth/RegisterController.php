<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    // RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [ 
            'name' => 'required', 
            'email' => 'required|email|unique:users', 
            'password' => 'required', 
            'gender' => 'required',
            'phone_number' => 'required',
            'password-confirm' => 'required|same:password', 
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        //assign all variable to the input
        $input = [];
        $input['name'] = $data['name'];
        $input['email'] = $data['email'];
        $input['password'] = $data['password'];
        $input['gender'] = $data['gender'];
        $input['phone_number'] = $data['phone_number'];
        $input['password'] = bcrypt($data['password']);

        if(isset($data['avatar'])) {
            $avatar = $request->file('avatar');
            $extension = $avatar->getClientOriginalExtension();
            $fileName = $avatar->getFilename();
            $destinationPath = '/images/avatar/'.$fileName.'.'.$extension;
            Storage::disk('public')->put($destinationPath,  File::get($avatar));
            $input['avatar_url'] = $destinationPath;
        }else if(isset($data["avatar_socmed"])){

            $input['avatar_url'] = $data['avatar_socmed'];
        }else{
                $input['avatar_url'] = null;
        }
            
        //if register via socmed
        if(isset($data['via']) )
        {
            if($data['via'] == 'twitter')
            {
                $input['twitter_id'] = $data['id'];
                $input['twitter_token'] = $data['token'];
                $input['twitter_token_secret'] = $data['secret'];
            }else if($data['via'] == 'google')
            {
                $input['google_id'] = $data['id'];
                $input['google_token'] = $data['token'];
            }else if($data['via'] == 'facebook')
            {
                $input['facebook_id'] = $data['id'];
                $input['facebook_token'] = $data['token'];
            }
        }

        // dd($input);
            
        // User::create($input);
        // dd($user);
        return User::create($input);
    }
}

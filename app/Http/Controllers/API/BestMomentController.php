<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User; 
use App\BestMoment;
use App\VideoJob;
use Illuminate\Support\Facades\Auth; 
use Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;


class BestMomentController extends Controller
{
    public function store(Request $request)
    {

    	$validator = Validator::make($request->all(), [ 
            'order' => 'required|integer|between:1,10', //only accept 1 - 10
            'image' => 'required|image|mimes:jpeg,png,jpg', //only accept jpg, png, jpeg
        ]);
    	if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $order = $request->input('order');
        $user = Auth::user();
        //check latest validated best-moment
        if($order != 1)//ignore if order 1
        {
        	$bestMoment = BestMoment::where('user_id',$user->id)->where('is_valid',1)->orderBy('order','DESC')->first();
        	//stop kalau user uploadnya lebih dari yang seharusnya
        	if($order > $bestMoment->order + 1)
        	{
        		return response()->json(['error'=>'Belum saatnya mengupload ini'], 403);
        	}
        }

        //get image that already active in the same order
        $bestMoment = BestMoment::where('user_id',Auth::user()->id)
        	->where('order',$request->input('order'))
        	->where('is_active',1)->first();
        
        //if found, set it to inactive
        if($bestMoment)
        {
        	$bestMoment->is_active = 0;
        	$bestMoment->save();
        }
        
        //prepare input
        $input = [];
        $input['order'] = $request->input('order');
        $input['user_id'] = Auth::user()->id;
        $input['is_active'] = 1;
        $input['is_valid'] = 0;

        //save image operation
        if($request->hasFile('image')) {
        	$avatar = $request->file('image');
		    $extension = $avatar->getClientOriginalExtension();
		    $fileName = $this->generateRandomString(50).strtotime('now');
		    $destinationPath = '/images/user-photo/'.Auth::user()->id.'/'.$fileName.'.'.$extension;
		    Storage::disk('public')->put($destinationPath,  File::get($avatar));
		    $input['image_url'] = $destinationPath;
    	}else{
    		$input['image_url'] = null;
    	}
    	$bestMoment = BestMoment::create($input);

    	//return allbesst moment by user that activated
    	$bestMoments = BestMoment::where('user_id',Auth::user()->id)
    	->where('is_active',1)
    	->get();
    	foreach ($bestMoments as $key => $value) {
    		if($value->is_valid != 1)
    		{
    			$bestMoments[$key]['image_url'] = null;
    		}
    	}
    	$success = $bestMoments;
    	return response()->json(['success'=>$success], 200); 
    }

    public function delete(Request $request, BestMoment $bestMoment)
    {
    	$bestMoment->is_active = false;
    	$bestMoment->save();

    	return $this->get($request);
    }

    public function get(Request $request)
    {
    	//return allbesst moment by user that activated
    	$bestMoments = BestMoment::where('user_id',Auth::user()->id)
    	->where('is_active',1)
    	->get();
    	foreach ($bestMoments as $key => $value) {
    		if($value->is_valid != 1)
    		{
    			$bestMoments[$key]['image_url'] = null;
    		}
    	}
    	$success = $bestMoments;
    	return response()->json(['success'=>$success], 200); 
    }

    public function getUnprocessedJob(Request $request)
    {
        $video = VideoJob::where('status',0)->orderBy('created_at','ASC')->first();
        if($video){
            $video->status = 2;
            $video->save();
        }
        return response()->json($video);
    }
    public function setCompleted(Request $request,VideoJob $videoJob)
    {
        $videoJob->status = 1;
        $videoJob->video_url = $request->get('url');
        $videoJob->save();
    }
    function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}


}

<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Socialite;

class UserController extends Controller
{
    public $successStatus = 200;

    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(){ 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')-> accessToken; 
            return response()->json(['success' => $success], $this-> successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }
	/** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|email', 
            'password' => 'required', 
            'gender' => 'required',
            'phone_number' => 'required',
            'c_password' => 'required|same:password', 
        ]);
		if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        //assign all variable to the input
        $input = [];
        $input['name'] = $request->input('name');
        $input['email'] = $request->input('email');
        $input['password'] = $request->input('password');
        $input['gender'] = $request->input('gender');
        $input['phone_number'] = $request->input('phone_number');
        $input['password'] = bcrypt($input['password']);

        if($request->hasFile('avatar')) {
        	$avatar = $request->file('avatar');
		    $extension = $avatar->getClientOriginalExtension();
		    $fileName = $avatar->getFilename();
		    $destinationPath = '/images/avatar/'.$fileName.'.'.$extension;
		    Storage::disk('public')->put($destinationPath,  File::get($avatar));
		    $input['avatar_url'] = $destinationPath;
    	}else if($request->has("avatar_socmed")){

    		$input['avatar_url'] = $request->input('avatar_socmed');
    	}else{
    			$input['avatar_url'] = null;
    	}
    		
    	//if register via socmed
    	if($request->has('via') )
    	{
    		if($request->input('via') == 'twitter')
    		{
    			$input['twitter_id'] = $request->input('id');
    			$input['twitter_token'] = $request->input('token');
    			$input['twitter_token_secret'] = $request->input('secret');
    		}else if($request->input('via') == 'google')
    		{
    			$input['google_id'] = $request->input('id');
    			$input['google_token'] = $request->input('token');
    		}else if($request->input('via') == 'facebook')
    		{
    			$input['facebook_id'] = $request->input('id');
    			$input['facebook_token'] = $request->input('token');
    		}
    	}
		
        $user = User::create($input); 
        $success['token'] =  $user->createToken('MyApp')-> accessToken; 
        $success['name'] =  $user->name;
		return response()->json(['success'=>$success], $this-> successStatus); 
    }
	/** 
	 * details api 
	 * 
	 * @return \Illuminate\Http\Response 
	 */ 
    public function details() 
    { 
        $user = Auth::user(); 
        return response()->json(['success' => $user], $this-> successStatus); 
    } 

    function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	/**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProvider($provider)
    {
        $user = Socialite::driver($provider)->user();
        
        if($provider == "facebook" || $provider == "twitter" || $provider == "google")
        {
        	//find corresponding user by email or fb id
        	if($user->email != null)
        	{
        		$old_user = User::where('email',$user->email)->first();
        	}else{
        		$old_user = User::where($provider.'_id',$user->id)->first();
        	}
        	if($old_user)
        	{
        		if($provider == "facebook")
        		{
        			$old_user->facebook_id = $user->id;
        			$old_user->facebook_token = $user->token;
        		}else if($provider == "twitter")
        		{
        			$old_user->twitter_id = $user->id;
        			$old_user->twitter_token = $user->token;
        			$old_user->twitter_token_secret = $user->token;
        		}else if($provider == "google")
        		{
        			$old_user->google_id = $user->id;
        			$old_user->google_token = $user->token;
        		}
        		$old_user->name = $user->name;
        		$old_user->avatar_url = $user->avatar;
        		$old_user->save();
        		$user = $old_user; 
        		Auth::login($user);
        		return redirect()->route('upload');
        		// $success['token'] =  $user->createToken('MyApp')-> accessToken; 
          //   	return response()->json(['success' => $success], $this-> successStatus);
        	}else{
        		$id = $user->id;
        		$name = $user->name;
        		$avatar_url = $user->avatar;
        		$via = $provider;
        		$token = $user->token;
        		if(isset($user->email)){
        			$email = $user->email;
        		}else{
        			$email = '';
        		}
        		if(isset($user->tokenSecret)){
        			$secret = $user->tokenSecret;
        		}else{
        			$secret = '';
        		}
        		return redirect()->route('register',compact(['id','name','avatar_url','via','token','secret','email']));
        		// return view('auth.register',compact(['id','name','avatar_url','via','token','secret','email']));
        	}
        }
        return 'error';
        // $user->token;
    }
}

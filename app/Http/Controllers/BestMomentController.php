<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User; 
use App\BestMoment;
use App\VideoJob;
use Illuminate\Support\Facades\Auth; 
use Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Image;
use App;


class BestMomentController extends Controller
{
    public function store(Request $request)
    {

    	$validator = Validator::make($request->all(), [ 
            'order' => 'required|integer|between:1,10', //only accept 1 - 10
            'image' => 'required|image|mimes:jpeg,png,jpg|max:10240', //only accept jpg, png, jpeg
        ]);

    	if ($validator->fails()) { 
            $errorString = '<ul>';
            // dd($validator->errors()->messages;
            foreach ($validator->errors()->messages() as $errorMsg) {
                foreach($errorMsg as $error)

                    $errorString .='<li>'. $error . '</li>';
            }
            $errorString .= '</ul>';
            return back()->with('error', $errorString);           
        }
        
        $order = $request->input('order');
        $user = Auth::user();

        //disable check order, user free upload order
        //check latest validated best-moment
        // if($order != 1)//ignore if order 1
        // {
        // 	$bestMoment = BestMoment::where('user_id',$user->id)->where('is_valid',1)->orderBy('order','DESC')->first();
        // 	//stop kalau user uploadnya lebih dari yang seharusnya
        // 	if($order > $bestMoment->order + 1)
        // 	{
        // 		return back()->with('error', 'belum saatnya mengupload bagian ini');
        // 	}
        // }

        //check that user have waiting for validation status
        $bestMoment = BestMoment::where('user_id',Auth::user()->id)
            ->where('is_active',1)
            ->where('is_valid',2)->first();
        // if(Auth::user()->id != 5 && Auth::user()->id != 1){
            // if($bestMoment && App::environment('production'))
            // {
                // return back()->with('error', 'kamu masih punya gambar yang belum divalidasi');
            // }
        // }

        //get image that already active in the same order
        $bestMoment = BestMoment::where('user_id',Auth::user()->id)
        	->where('order',$request->input('order'))
        	->where('is_active',1)->first();
        
        //if found, set it to inactive
        if($bestMoment)
        {
        	$bestMoment->is_active = 0;
        	$bestMoment->save();
        }
        
        //prepare input
        $input = [];
        $input['order'] = $request->input('order');
        $input['user_id'] = Auth::user()->id;
        $input['is_active'] = 1;
        $input['is_valid'] = 2;

        //save image operation
        if($request->hasFile('image')) {
        	$avatar = $request->file('image');
		    $extension = $avatar->getClientOriginalExtension();
		    $fileName = $input['order'];
		    $destinationPath = '/images/user-photo/'.Auth::user()->id.'/'.$fileName.'.'."jpg";
            Storage::disk('public')->put($destinationPath,  File::get($avatar));
            $jpg = Image::make('storage'.$destinationPath);
            $jpg->orientate();
            $jpg->fit(640,480);
            $jpg->save('storage'.$destinationPath,80);

		    $input['image_url'] = $destinationPath;
    	}else{
    		$input['image_url'] = null;
    	}
        if(App::environment(['local','staging']))
        {
            $input['is_valid'] = 1;
        }
    	$bestMoment = BestMoment::create($input);
    	return redirect()->back();
    }
    public function delete(Request $request, $bestMoment)
    {
    	$bestMoment = BestMoment::find($bestMoment);
    	if($bestMoment){
	    	if($bestMoment->user_id == Auth::user()->id)
	    	{
	    		$bestMoment->is_active = false;
	    		$bestMoment->save();	
	    	}
    	}
    	
    	return redirect()->back();
    }
    public function toggleLike(Request $request,$post)
    {
        $user = Auth::user();
        $video = $request->get('video');

        if($video == 'true')
        {
            $post = VideoJob::where('id',$post)->first();
        }else{

            $post = BestMoment::where('id',$post)->first();

        }
        $user->toggleLike($post);

        return response()->json([
            'is_liked'=> $post->isLikedBy($user),
            'id' =>$post->id,
            'total_like' => $post->likers()->count()
        ]);
        // return redirect()->back();
    }
    function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
}

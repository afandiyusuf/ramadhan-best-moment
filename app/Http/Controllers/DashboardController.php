<?php

namespace App\Http\Controllers;
use App\BestMoment;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\VideoJob;
class DashboardController extends Controller
{
    public function index(Request $request, User $user)
    {

        $self = Auth::user();
        $self_feed = false;
        if($user->id == $self->id)
        {
        	$self_feed = true;
        }
        $videoJob = null;
        $videoJob = VideoJob::where('user_id',$user->id)->where('status',1)->first();
    	$feeds = BestMoment::where('is_valid',1)
    		->where('user_id',$user->id)->where('is_active',1)
    		->orderBy('updated_at','DESC')->get();
    	
    	return view('feeds',compact(['feeds','self','self_feed','videoJob']));
    }
}

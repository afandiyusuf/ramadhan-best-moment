<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Overtrue\LaravelLike\Traits\Likeable;

class BestMoment extends Model
{
    use Likeable;
    
    protected $fillable = [
        'order', 'user_id', 'image_url','is_valid','is_active'
    ];
    public function user(){
    	return $this->belongsTo('App\User');
    }
    public function status(){
    	if($this->is_valid == 2)
    	{
    		return 'menunggu diperiksa oleh admin';
    	}else if($this->is_valid == 0)
    	{
    		return 'gambarmu ditolak, coba kirim lagi';
    	}else{
    		return 'aktif';
    	}
    }
}


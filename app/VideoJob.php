<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Overtrue\LaravelLike\Traits\Likeable;

class VideoJob extends Model
{
	use Likeable;
	
    protected $table = 'video_jobs';
    protected $fillable = [
    	'user_id','status','video_url'
    ];
    public function user(){
    	return $this->belongsTo('App\User');
    }
}

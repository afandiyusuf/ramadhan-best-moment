<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'API\UserController@login');

Route::post('register', 'API\UserController@register');
Route::group(['middleware' => 'auth:api'], function(){
	Route::post('details', 'API\UserController@details');
});
Route::group(['prefix'=>'best-moment','middleware'=>'auth:api'], function(){
	Route::get('','API\BestMomentController@get');
	Route::post('store','API\BestMomentController@store');
	Route::post('delete/{bestMoment}','API\BestMomentController@delete');
});
Route::get('get-unprocessed-job','API\BestMomentController@getUnprocessedJob');
Route::get('set-completed/{videoJob}','API\BestMomentController@setCompleted');

<?php

use Illuminate\Support\Facades\Route;
App::setLocale('id');
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',function(){
	if(!Auth::user()){
		return view('index');
	}else
		return redirect()->route('upload');
})->name('index');
Route::get('/privacy-policy',function(){
	return view('privacy_policy');
});

Auth::routes();
Route::group(['middleware'=>'web'],function(){
	Route::get('/logout', function(){
		Auth::logout();
		return redirect()->route('index');
	});
	Route::get('login/{provider}', 'API\UserController@redirectProvider')->name('login.provider');
	Route::get('login/{provider}/callback', 'API\UserController@handleProvider')->name('register.provider');
});
Route::group(['middleware'=>['auth','web']],function(){
	Route::get('/home', 'HomeController@index')->name('upload');
	// Route::get('/profile','DashboardController@index')->name('feeds');
	Route::get('/profile/{user}','DashboardController@index')->name('feeds');
	Route::post('/best-moment/store','BestMomentController@store')->name('best-moment.store');
	Route::post('/best-moment/delete/{bestMoment}','BestMomentController@delete')->name('best-moment.delete');
	Route::post('/best-moment/toggle-like/{post?}','BestMomentController@toggleLike')->name('toggle-like');
});

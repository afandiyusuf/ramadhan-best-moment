<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGenderColumnAndOtherColumnAtUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table){
            $table->tinyInteger('gender')->default(0);
            $table->string('avatar_url')->nullable();
            $table->string('phone_number');
            $table->tinyInteger('user_type')->default(1);
            $table->boolean('blocked')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table){
            $table->dropColumn('gender');
            $table->dropColumn('avatar_url');
            $table->dropColumn('phone_number');
        });
    }
}

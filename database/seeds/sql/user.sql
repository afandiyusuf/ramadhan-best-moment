INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `gender`, `avatar_url`, `phone_number`, `user_type`, `blocked`) VALUES
(1, 'yusuf_afandi', 'afandi.yusuf.04@gmail.com', NULL, '$2y$10$S9Tv/YqyK7hSU4VUYekdje2FvV6de15J2k8tK31UTOvsiIWvFHFV.', NULL, '2020-04-24 05:20:23', '2020-04-25 17:50:17', 0, NULL, '', 1, 0),
(3, 'yusuf_afandi', 'afandi.yusuf.04@gmail.com2', '2020-04-25 02:42:21', '$2y$10$d4ZMrL4E4qrJMa1ONZXXTOm4mEQQQdH1eBDqvtcQkQWAwZuDJBW16', NULL, '2020-04-24 06:08:32', '2020-04-25 02:42:24', 1, '/images/avatarphp74F4.tmp.png', '085636352452', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

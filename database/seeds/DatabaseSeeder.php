<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
    	//user
        $this->call(OrderSeeder::class);
    	$user = base_path('database/seeds/sql/user.sql');
    	$menu = base_path('database/seeds/sql/admin.sql');
        \DB::unprepared(file_get_contents($user));
        \DB::unprepared(file_get_contents($menu));
        
       
    }
}

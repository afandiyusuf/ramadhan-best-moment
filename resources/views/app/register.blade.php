<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	<style>
		.overlay{
			width: 100vw;
			height: 100vh;
			background-color: black;
			opacity: 0.5;
			display: none;
		}
	</style>
</head>
<body>
	<div class="overlay" id="overlay"></div>
	<div>
		<input type="hidden" name="id" id="id" value="{{$id ?? ''}}"/>
		<input type="hidden" name="token" id="token" value="{{$token ?? ''}}"/>
		<input type="hidden" name="secret" id="secret" value="{{$secret ?? ''}}"/>
		<input type="hidden" name="via" id="via" value="{{$via ?? ''}}"/>
		<input type='hidden' name="avatar-socmed" id="avatar_socmed" value="{{$avatar_url ?? ''}}"/>
  <div class="form-group">
    <label for="input-email">Email</label>
    <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Masukkan Email" value="{{ $email ?? '' }}" 
    @if(isset($email)) 
    	readonly 
    @endif >
    
  </div>
  <div class="form-group">
    <label for="input-email">Name</label>
    <input type="text" class="form-control" name="name" id="name" aria-describedby="emailHelp" placeholder="Masukkan Nama" value="{{ $name ?? '' }}" >
    
  </div>
  <div class="form-group">
    <label for="password">Password</label>
    <input type="password" name="password" class="form-control" id="password" placeholder="Password">
  </div>
  <div class="form-group">
    <label for="input-confirm-password">Confirm Password</label>
    <input type="password" name="c_password" class="form-control" id="c_password" placeholder="Masukkan Password">
  </div>

  <div class="form-group">
  	<div class="preview-avatar">
  		<img src="{{$avatar_url ?? ''}}"/>
  	</div>

    <label for="avatar">Avatar</label>
    <input type="file" class="form-control-file" id="avatar">
  </div>
  
  <div class="form-group">
    <label for="input-gender">Gender</label>
    <select name ="gender" class="form-control" id="gender">
      <option value='1'>Laki-laki</option>
      <option value='2'>Perempuan</option>
    </select>
  </div>
  <div class="form-group">
    <label for="input-phone">Phone Number</label>
    <input type="text" class="form-control" id="phone_number" name="phone_number" aria-describedby="emailHelp" placeholder="Masukkan no telepon" value="">
  </div>
  <button onclick="submitThis()" class="btn btn-primary">Submit</button>
</div>
<script src="https://code.jquery.com/jquery-3.5.0.min.js" integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ=" crossorigin="anonymous"></script>
<script type="text/javascript">
	function submitThis()
	{
		$("#overlay").css('display','block');
		var name = $("#name").val();
		var email = $("#email").val();
		var password = $("#password").val();
		var c_password = $("#c_password").val();
		var gender = $("#gender").val();
		var phone_number = $("#phone_number").val();
		var id = $("#id").val();
		var token= $("#token").val();
		var secret = $("#secret").val();
		var via = $("#via").val();
		var avatar_socmed = $("#avatar_socmed").val();

		if(name != "" && email != "" && password != "" && c_password != "" && gender != "" && phone_number != "")
		{
			var fd = new FormData();
			var avatar = $('#avatar')[0].files[0];
			fd.append('avatar',avatar);
			fd.append('name',name);
			fd.append('email',email);
			fd.append('password',password);
			fd.append('c_password',c_password);
			fd.append('gender',gender);
			fd.append('phone_number',phone_number);
			fd.append('id',id);
			fd.append('token',token);
			fd.append('secret',secret);
			fd.append('via',via);
			fd.append('avatar_socmed',avatar_socmed);

			$.ajax({
			  url: "/api/register",
			  data: fd,
			  enctype: 'multipart/form-data',
			  method: 'POST',
			  contentType: false,
        	  processData: false,
        	  success:function(data){
        	  	window.localStorage.setItem('token',data.success.token);
        	  	window.location.href = '/gallery';
        	  	$("#overlay").css('display','none');
        	  },
        	  error:function(err){
        	  	$("#overlay").css('display','none');
        	  }
        	});

		}else{
			$("#overlay").css('display','none');
		}
	}
</script>
</body>
</html>
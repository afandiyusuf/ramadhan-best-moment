<!doctype html>
<html lang="id">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="favicon.ico">
    <meta property="og:title" content="{{__('share title')}}">
    <meta property="og:description" content="{{__('share description')}}">
    <meta property="og:image" content="http://euro-travel-example.com/thumbnail.jpg">
    @guest
    <meta property="og:url" content="{{route('index')}}">
    @else
    <meta property="og:url" content="{{route('feeds',['user'=>Auth::user()->id])}}">
    @endguest
    <meta property="fb:app_id" content="859445574083747" />

    <meta name="twitter:title" content="{{__('share title')}}">
    <meta name="twitter:description" content="{{__('share description')}}">
    <meta name="twitter:image" content=" http://euro-travel-example.com/thumbnail.jpg">
    <meta name="twitter:card" content="{{__('share description')}}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <!-- Styles -->
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
    {{-- <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet"> --}}
    <link href="{{asset('css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-social.css') }}" rel="stylesheet">
    @yield('style')
    <style>
        @font-face {
            font-family: 'cookies';
            src: url("{{asset('fonts/cookies.woff')}}"); 
        }
         @font-face {
            font-family: 'viga';
            src: url("{{asset('fonts/Viga-Regular.woff')}}"); 
        }
        .general{
            display: table;
        }
        .potrait{
            display: none;
        }
        html,body{
            font-family: 'viga';
            height: 100%;
            width: 100%;
            
        }
        
        .background-image{
            background-image:url('{{asset('images/background-pc-edit.png')}}');
            background-size: cover ;
            /*background-repeat: no-repeat;*/
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: -100;
        }

        .maskot{
            height: 50vh;
            width: 16vw;

        }
        .maskot-left{
            position: fixed;
            left: 5vw;
            bottom: 0px;
            background-image:url('{{asset('images/maskot-left.png')}}');
            background-size: contain;
            background-repeat: no-repeat;
            background-position: bottom left;
        }
        .maskot-right{
            position: fixed;
            right: 5vw;
            bottom: 0px;
            background-image:url('{{asset('images/maskot-right.png')}}');
            background-size: contain;
            background-repeat: no-repeat;
            background-position: bottom right;
        }
        .navbar{
            position: fixed;
            width: 100vw;
            background-color: #113863;
            color:white;
            height: 60px;
            z-index: 1999999;
        }
        .nav-link a{
            color:white;
        }
        .navbar-brand{
            position: absolute;
            left:10vw;
            height: 60%;
            /*padding: 0.2em;*/

        }
         .btn-access{
            background-color: white;
            border-color:  #91C846 !important;
        }
        #dropdown-menu{
            position: absolute;
            right: 10vw;
            float:right;
            transform: translateX(-50%);
            cursor: pointer;

        }
        .navbar-brand{
            float:left;
        }
        .dropdown-expand{
            position: fixed;
            top:-200px;
            right: 10vw;
            background-color:white;
            padding: 20px;
            transition: 0.5s;
            z-index:999999;
        }
        .show{
            transition: 0.5s;
            top:60px;
        }
        .dropdown-expand a{
            color:#91C846;
        }
        @media (orientation: portrait) {
            .background-image{
                background-image:url('{{asset('images/background-mobile.png')}}');
                background-size: cover !important;
                background-position: bottom center;


            }
            .maskot{
                height: 20vh;
                width: 40vw;
                background-position: bottom center;
                background-size: contain;
                background-repeat: no-repeat;
            }
            .maskot-left{
                background-position: top left;
            }
            .maskot-right{
                background-position: top right;
            }
            .navbar{
                width: 100vw;
            }
            .general{
                display: none;
            }
            .potrait{
                display: table;
            }
          
        }
        .d-flex{
            align-items: baseline !important;
        }
    </style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-61831555-3"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-61831555-3');
    </script>
    
</head>
<body>
    @include('shared.errors')

    <div class='background-image'></div>
    <div>    
        <nav class="navbar navbar-expand-md shadow-sm mt-0">
            
            
            @auth
            <a class="navbar-brand" href="{{ route('upload') }}">
                <img height="100%" src="{{asset('images/navbar-icon.png')}}"/>
            </a>
            <div id="dropdown-menu">
                <span class="fa fa-bars"></span>
            </div>
            @else
             <a class="navbar-brand" href="{{ route('index') }}">
                <img height="100%" src="{{asset('images/navbar-icon.png')}}"/>
            </a>
            @endauth

        </nav>
        @auth
        <div class="dropdown-expand">
            <div class="dropdown-greeting">
            Halo, {{Auth::user()->name}}
            </div>
            <hr/>

            @if(Route::current()->getName() == 'upload')
            <a href="{{route('feeds',['user'=>Auth::user()->id])}}">{{__('Feed')}}</a>
            @else
            <a href="{{route('upload')}}">{{__('Upload')}}</a>
            @endif
            <hr/>
            <a href="{{route('logout')}}">{{__('Logout')}}</a>
           
        </div>
        @endauth
        <main class="py-4">
            @yield('content')
        </main>
       
    </div>
    <div class="maskot maskot-left">
    </div>
    <div class="maskot maskot-right">
    </div>
    <script type="text/javascript">
        $("#dropdown-menu").on('click',function(){
            $(".dropdown-expand").toggleClass('show');
        });
    </script>
    @yield('script')


</body>
</html>

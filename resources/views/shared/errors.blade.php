 @if (Session::has('error') || $errors->any())
<div class="alert alert-danger alert-dismissible fade show" style="
 		position: fixed;
 		top: 0;
 		left:0;
 		right: 0;
 		z-index: 1000000000;
" role="alert">
  <strong>{!! Session::get('error') !!}</strong>
  <ul>
	@foreach ($errors->all() as $error)
	    <li>{{ $error }}</li>
	@endforeach
</ul>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="d-flex justify-content-center brand-info-area">
        <div class="fixed-content">
            <div class="d-flex justify-content-center brand-logo">
                <img height="60px" class="brand-logo-image" src="{{asset('images/landing-brand.png')}}"/>
            </div>
            
            <div class="d-flex justify-content-center social-btn-container">
                <a href="{{route('upload')}}" class="label">Selamat datang, {{Auth::user()->name}}.
                upload foto kamu!</a>
            </div>
            <div class="d-flex justify-content-center caption-logo-container">
                <img class="hashtag-logo" src="{{asset('images/hashtag.png')}}"/>
            </div>
        </div>
    </div>
    <div class="container card-section col-md-6 feed-container" id="card-section">
        <div class="d-flex justify-content-center">
            <div class="card card-container col-md-12">
                <div class="card-body section-card-container">
                    @if(count($feeds) == 0)
                        <div class="user-section">
                            <div class="card card-content">
                                <div class="card-body">
                                    @if($self_feed)
                                    <a href="{{route('upload')}}" class="label-poster">
                                    {{__("Feed Kosong Sendiri")}}</a>
                                    @else
                                    <a href="{{route('feeds',['user'=>$self->id])}}" class="label-poster">
                                    {{__("Feed Kosong Orang Lain")}}</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endif
                    {{-- USER SECTION START HERE --}}
                    @if($videoJob != null)
                    @php $feed = $videoJob @endphp
                        <div class="user-section">
                        <div class="card card-content">
                            <div class="card-body">
                                <div class="label-poster"><strong>{{$feed->user->name}}</strong>, {{$feed->updated_at->diffForHumans()}}</div>
                            </div>
                        </div>
                        <div class="card card-content card-image-content">
                            <div class="card-body">
                                <video class="image-user" controls>
                                    <source src="{{$feed->video_url}}" type="video/mp4"/>
                                </video>
                            </div>
                        </div>
                        <div class="card card-content card-like col-md-12">
                            <div class="card-body row section-card">
                                <div class="like-area col-md-6">
                                   
                                    @if($feed->isLikedBy($self))
                                    <div class="fa fa-heart button-like" id="{{$feed->id}}" post-id="{{$feed->id}}" onclick="toggleLike(this)"></div><span class="like-text" id="like-text-{{$feed->id}}" post-id="{{$feed->id}}">Like ({{$feed->likers()->count()}})</span>
                                    @else
                                    <div class="fa fa-heart-o button-like" id="{{$feed->id}}" post-id="{{$feed->id}}" onclick="toggleLike(this)"></div><span class="like-text" id="like-text-{{$feed->id}}" post-id="{{$feed->id}}">Like ({{$feed->likers()->count()}})</span>
                                    @endif

                                    
                                    
                                </div> 
                                @if($self_feed)
                                <div class="share-area">
                                    <div class="col-md-12 share-content">Share 
                                        <a class="btn btn-flex btn-social-icon btn-twitter white" 
                                                href="https://twitter.com/intent/tweet?text={{__('copytext share twitter')}} {{route('feeds',['user'=>$self->id])}}" target="_blank">
                                                <span class="fa fa-twitter white"></span>
                                        </a>
                                        <a class="btn btn-flex btn-social-icon btn-facebook white" 
                                                href="https://www.facebook.com/dialog/feed?
  app_id=859445574083747&display=popup&link={{route('feeds',['user'=>$self->id])}}&redirect_uri={{route('feeds',['user'=>$self->id])}}">
                                                <span class="fa fa-facebook white"></span>
                                        </a>
                                        <a class="btn btn-flex btn-social-icon btn-whatsapp white" 
                                                href="https://api.whatsapp.com/send?text={{__('copytext share wa')}} {{route('feeds',['user'=>$self->id])}}" target="_blank">
                                                <span class="fa fa-whatsapp white"></span>
                                        </a>
                                       
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    
                    @endif
                    @foreach($feeds as $feed)
                    <div class="user-section">
                        <div class="card card-content">
                            <div class="card-body">
                                <div class="label-poster"><strong>{{$feed->user->name}}</strong>, {{$feed->updated_at->diffForHumans()}}</div>
                            </div>
                        </div>
                        <div class="card card-content card-image-content">
                            <div class="card-body">
                                <div class="image-user" style="background-image: url('{{asset('storage'.$feed->image_url)}}'); background-size:contain; background-repeat:no-repeat; background-position:center">
                                    
                                </div>
                            </div>
                        </div>
                        <div class="card card-content card-like col-md-12">
                            <div class="card-body row section-card">
                                <div class="like-area col-md-6">
                                   
                                    @if($feed->isLikedBy($self))
                                    <div class="fa fa-heart button-like" id="{{$feed->id}}" post-id="{{$feed->id}}" onclick="toggleLike(this)"></div><span class="like-text" id="like-text-{{$feed->id}}" post-id="{{$feed->id}}">Like ({{$feed->likers()->count()}})</span>
                                    @else
                                    <div class="fa fa-heart-o button-like" id="{{$feed->id}}" post-id="{{$feed->id}}" onclick="toggleLike(this)"></div><span class="like-text" id="like-text-{{$feed->id}}" post-id="{{$feed->id}}">Like ({{$feed->likers()->count()}})</span>
                                    @endif

                                    
                                    
                                </div> 
                                @if($self_feed)
                                <div class="share-area">
                                    <div class="col-md-12 share-content">Share 
                                        <a class="btn btn-flex btn-social-icon btn-twitter white" 
                                                href="https://twitter.com/intent/tweet?text={{__('copytext share twitter')}} {{route('feeds',['user'=>$self->id])}}" target="_blank">
                                                <span class="fa fa-twitter white"></span>
                                        </a>
                                        <a class="btn btn-flex btn-social-icon btn-facebook white" 
                                                href="https://www.facebook.com/dialog/feed?
  app_id=859445574083747&display=popup&link={{route('feeds',['user'=>$self->id])}}&redirect_uri={{route('feeds',['user'=>$self->id])}}">
                                                <span class="fa fa-facebook white"></span>
                                        </a>
                                        <a class="btn btn-flex btn-social-icon btn-whatsapp white" 
                                                href="https://api.whatsapp.com/send?text={{__('copytext share wa')}} {{route('feeds',['user'=>$self->id])}}" target="_blank">
                                                <span class="fa fa-whatsapp white"></span>
                                        </a>
                                       
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    @endforeach
                    {{-- END USER SECTION --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="https://code.jquery.com/jquery-3.5.0.min.js" integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ=" crossorigin="anonymous"></script>
<script type="text/javascript">
    $.ajaxSetup({

        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }

    });

    function toggleLike(val,video=false)
    {
        let id = val.getAttribute('post-id');
        // $("#"+id).removeClass('fa-heart-o');
        // $("#"+id).removeClass('fa-heart');
        $("#"+id).addClass('fade-out');
        let url = '{{route('toggle-like')}}/'+val.getAttribute('post-id');
        if(video)
        {
            url += "?video=true";
        }else{
            url += "?video=false";
        }
        $.ajax({

           type:'POST',

           url:url,
           success:function(data){
            let id = data.id;
            let total_like = data.total_like;
            let is_liked = data.is_liked;
            $("#"+id).removeClass('fa-spin');
            if(is_liked){

                $("#"+id).removeClass('fa-heart-o');
                $("#"+id).addClass('fa-heart');
            }else{
                $("#"+id).removeClass('fa-heart');
                $("#"+id).addClass('fa-heart-o');
            }
            $("#"+id).removeClass('fade-out');
            $("#like-text-"+id).html('Like('+total_like+')');

           }

        });
    }
</script>
@endsection

@section('style')
<style>
    @guest
    @else
    html body{
        height: 100%;
        overflow: hidden;
    }
    @endguest
.fade-out{
    transform: scale(3) !important;
    transition: 0.5s !important;
    opacity: 0;
}

.section-card{
    padding-left: 5px;
    padding-right: 5px;
}
.line-it-button{
    vertical-align: middle !important;
}
#card-section::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #1888B8;
}

#card-section::-webkit-scrollbar
{
    width: 6px;
    background-color: #F5F5F5;
}

#card-section::-webkit-scrollbar-thumb
{
    background-color: white;
}

.like-area{
    position: absolute;
    left: 0;
    top: 50%;
    transform: translateY(-50%);
}
.share-area{
    position: absolute;
    right: 0;
    top: 50%;
    transform: translateY(-50%);
}


    .btn-whatsapp{
        background-color: #2CC64E;
    }
    
    .card-section{
        margin-top: 34vh;
        overflow-y: auto;
        height: 66vh;
        min-width: 540px;
    }
    .fixed-content{
        position: fixed;
        z-index: 1000;
        top:3vh;
    }
    .brand-logo{
        margin-top: 10vh;
    }
    .share-content{
        text-align: right;
    }
    .card-like{
        margin-top: 10px;
        height: 60px;
    }
    .user-section{
        margin-top: 10px;
    }
    .label-poster{
        color:#7DB538;
    }
    .card-container{
        margin-top: 5px;
        background-color: #7DB538;
        padding:10px;
    }
    .button-like{   
        transform: scale(1);
        transition: 0.5s;
        color:#7DB538 !important;
    }
    .button-like:hover{
        cursor: pointer;
    }
    .like-text{
        font-family: 'viga';
        color:#7DB538 !important;
        margin-left: 5px;
    }
    .card-content{
        background-color: #EBEBE9;
    }
    .card-image-content{
        margin-top: 10px;
    }
    .image-user{
        height: 300px;
    }
    .label{
        color:white !important;
    }
    .white{
        color:white !important;
    }
    .main-logo-container{
        margin-top: 5vh;
    }
    .landing-main-logo{
        height: 30vh;
    }
    .caption-logo-container{
        margin-top: 2vh;
    }
    .landing-caption-logo{
        height: 10vh;
    }
    .social-btn-container{
        margin-top: 2vh;
    }
    .btn-social{

        width: 15vw;
        color: white !important;
        margin:auto;
    }
    .btn-social-icon{

    }
    .btn-login{
        width: 7.2vw;
        margin-right: 0.3vw;
    }
    .btn-register{
        width: 7.2vw;
        margin-left: 0.3vw;
    }

    .hashtag-logo{
        width: 30vw;
    }
    .login-register-container{
        margin-bottom: 2vh;
        color:#91C846 !important;
    }

   a {
        color:#91C846 !important;
    }
    .btn-access:hover{
        background-color: #91C846;
        color: white !important;
    }
    .btn-access:active{
        background-color: #91C846 !important;
        color: white !important;
    }
    .feed-container{
        padding-bottom: 30vh;
    }
    @media (orientation: portrait) {
        html,body{
            height: 100%;
            overflow-y: hidden;
        }
        .brand-logo-image{
            width: 65vw;
            height: auto !important;
        }
       .brand-logo{
            margin-top: 8vh;
       }
       .card-section{
            min-width: auto;
            width: 90vw;
            margin-top: 22vh;
            height:78vh;
            padding-left:0px;
       }
       .image-user{
            height: 50vw;
       }
       .card-body{
            padding:0.5rem;
       }
       .like-area{
            font-size:3.5vw;
       }
        .maskot{
            /*height: 20vh !important;*/
            width: 20vw !important;
            background-size: contain !important;
            bottom: 0;  
        }
        .maskot-left{
            background-position: bottom left !important;
            left:10px !important;
        }
        .maskot-right{
            background-position: bottom right !important;
            right:10px !important;
        }
        .btn-social-icon{
            font-size: 2vw !important;
            line-height: auto !important;
            width: 7vw;
            height: 7vw;
            top:50%;
            /*transform:translateY(-50%);*/
        }
        .btn-social-icon>:first-child{
            line-height: normal !important;
            transform: translateY(-50%);
            margin-top: 50%;
        }
        .social-btn-container{
            width: 90vw;
            text-align: center;
            font-size: 4vw;
        }
        .section-card-container{
            padding-left: 0px;
            padding-right: 0px;
        }
        .feed-container{
            width: auto !important;
            padding-right: 1px;
            padding-left: 1px;
            padding-bottom: 30vh;
        }
        
    }
    @media (orientation: landscape) {
        .brand-logo-image{
            height: 10vh;
            width: auto;
            margin-top: 10px;
        }
        .card-section{
            margin-top: 40vh;
        }
    }
</style>
@endsection
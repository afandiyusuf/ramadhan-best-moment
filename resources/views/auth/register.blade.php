@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 form-container">
            <div class="card">                
                <div class="card-body">
                    <div class="d-flex justify-content-center title-container">
                        <div class="title">{{ __('Register') }}</div>
                    </div>
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <input type="hidden" name="id" id="id" value="{{request('id') ?? ''}}"/>
                        <input type="hidden" name="token" id="token" value="{{request('token') ?? ''}}"/>
                        <input type="hidden" name="secret" id="secret" value="{{request('secret') ?? ''}}"/>
                        <input type="hidden" name="via" id="via" value="{{request('via') ?? ''}}"/>
                        <input type='hidden' name="avatar-socmed" id="avatar_socmed" value="{{request('avatar_url') ?? ''}}"/>
        
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ request('name') ?? old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ request('email') ?? old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        

                        <div class="form-group row">
                            <div class="preview-avatar col-md-4 col-form-label text-md-right">
                                <img id="preview" width="80px" src="{{request('avatar_url') ?? ''}}"/>
                            </div>
                            <div class="col-md-6">
                                <label for="avatar">Avatar</label>
                                <input type="file" class="form-control-file" id="avatar" onchange="readURL(this)" >
                            </div>
                             @error('avatar')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="form-group row">
                            <label for="gender" class="col-md-4 col-form-label text-md-right">Gender</label>
                            <div class="col-md-6">
                                <select name ="gender" class="form-control" id="gender">
                                  <option value='1'>Laki-laki</option>
                                  <option value='2'>Perempuan</option>
                                </select>
                            </div>
                             @error('gender')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="form-group row">
                            <label for="input-phone" class="col-md-4 col-form-label text-md-right">Phone Number</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="phone_number" name="phone_number" aria-describedby="emailHelp" placeholder="Masukkan no telepon" value="" required>
                            </div>
                             @error('phone_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>    

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password-confirm" required autocomplete="new-password">
                            </div>
                             @error('password-confirm')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="form-group row">
                            
                        </div>
                        <div class="justify-content-center social-btn-container col-md-6 offset-md-4 pl-0 pr-0">
                            <button type="submit" class="btn btn-primary col-md-12 mb-2" id="button-register">
                                {{ __('Register') }}
                            </button>
                            <a class="btn btn-block btn-social btn-google white"
                                href="{{route('login.provider',['provider'=>'google'])}}" >
                                <span class="fa fa-google white"></span> {{__('Login with google')}}
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#preview')
                .attr('src', e.target.result).
                width('80px');
        };

        reader.readAsDataURL(input.files[0]);
    }
}
</script>
@endsection

@section('style')
<style>
    .card{
        margin:auto;
        margin-top: 10vh;
    }
 .title{
    font-family: 'viga';
    font-style: bold;
    color:#91C846;
    font-size: 30px;
}
.title-container{
    margin-top: 10px;
    margin-bottom: 10px;
}
#button-register {
    color:#91C846 !important;
    background-color: white;
    border-color: #91C846;
}
#button-register:hover{
    background-color: #91C846;
    color: white !important;
}
#button-register:active{
    background-color: #91C846 !important;
    color: white !important;
}

 @media(orientation:portrait){
        .maskot{
                /*height: 20vh !important;*/
                width: 20vw !important;
                background-size: contain !important;
                bottom: 0;  
            }
            .maskot-left{
                background-position: bottom left !important;
                left:10px !important;
            }
            .maskot-right{
                background-position: bottom right !important;
                right:10px !important;
            }
            .card{
                width: 80vw;
                margin-bottom: 100px;
            }
            .form-container{
                width:90% !important;
            }
    }

</style>
@endsection
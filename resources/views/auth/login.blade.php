@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6 form-container">
            <div class="card section-container">
{{--                 <div class="card-header">{{ __('Login') }}</div>
 --}}
                <div class="card-body">
                    <div class="d-flex justify-content-center title-container">
                        <div class="title">LOGIN</div>
                    </div>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-12 col-form-label">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-12 col-form-label">{{ __('Password') }}</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary col-md-12" id="button-login">
                                    {{ __('Login') }}
                                </button>
                                 <a href="{{route('register')}}"class="btn btn-primary col-md-12" id="button-register">
                                    {{ __('Register') }}
                                </a>

                               {{--  @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif --}}
                            </div>
                        </div>
                        <div class="justify-content-center social-btn-container">
                           
                            <a class="btn btn-block btn-social btn-google white"
                                href="{{route('login.provider',['provider'=>'google'])}}" >
                                <span class="fa fa-google white"></span> {{__('Login with google')}}
                            </a>
                           
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('style')
<style>
    .section-container{
        margin:auto;
    }
    .title{
        font-style: bold;
        color:#91C846;
        font-size: 30px;
    }
    .title-container{
        margin-top: 0px;
        margin-bottom: 0px;
    }
    .card{
        margin-top: 10vh;
        padding-left: 30px;
        padding-right: 30px;
    }
    .social-btn-container{
        margin-bottom: 20px;
    }
    #button-login {
        color:#91C846 !important;
        background-color: white;
        border-color: #91C846;
        margin-bottom: 10px;
    }
    #button-login:hover{
        background-color: #91C846;
        color: white !important;
    }
    #button-login:active{
        background-color: #91C846 !important;
        color: white !important;
    }
    #button-register {
    color:#91C846 !important;
    background-color: white;
    border-color: #91C846;
    }
    #button-register:hover{
        background-color: #91C846;
        color: white !important;
    }
    #button-register:active{
        background-color: #91C846 !important;
        color: white !important;
    }

    @media(orientation:portrait) and (max-height: 700px){
        .maskot{
                /*height: 20vh !important;*/
                width: 40vw !important;
                height: 30vh;
                background-size: contain !important;
                bottom: 0;  
            }
            .maskot-left{
                background-position: bottom left !important;
                left:10px !important;
            }
            .maskot-right{
                background-position: bottom right !important;
                right:10px !important;
            }
            .card{
                width: 80vw;
                margin-bottom: 100px;
            }
         
    }
    
</style>
@endsection

@extends('layouts.app')

@section('content')
 @if($self->completed==1)
<div class="overlay-finished">
    <div class="d-flex justify-content-center">
        <div class="selamat-img"
            style="background-image:url({{asset('images/selamat.png')}}" 
        >
       
            <div class="share-area">
                <div class="col-md-12 share-content"><img class="share-image" src="{{asset('images/share.png')}}"/> 
                    <a class="btn btn-flex btn-social-icon btn-twitter white" 
                            href="https://twitter.com/intent/tweet?text={{__('copytext share twitter')}} {{route('feeds',['user'=>$self->id])}}" target="_blank">
                            <span class="fa fa-twitter white"></span>
                    </a>
                    <a class="btn btn-flex btn-social-icon btn-facebook white" 
                            href="https://www.facebook.com/dialog/feed?
app_id=859445574083747&display=popup&link={{route('feeds',['user'=>$self->id])}}&redirect_uri={{route('feeds',['user'=>$self->id])}}">
                            <span class="fa fa-facebook white"></span>
                    </a>
                    <a class="btn btn-flex btn-social-icon btn-whatsapp white" 
                            href="https://api.whatsapp.com/send?text={{__('copytext share wa')}} {{route('feeds',['user'=>$self->id])}}" target="_blank">
                            <span class="fa fa-whatsapp white"></span>
                    </a>
                   
                </div>
                @if($videoJobStatus != 1)
                    <div class="video-status">video kamu sedang diproses</div>
                @else
                    <a href="{{route('feeds',['user'=>$self->id])}}" class="video-status">video kamu sudah selesai, share atau cek di sini </a>
                @endif
            </div>
            
        </div>
    </div>
</div>
@endif
<div class="container home-section">
    <div class="d-flex justify-content-center">
        <img class="home-main-logo general" src="{{asset('images/home-main-logo.png')}}"/>
        <img class="home-main-logo potrait" src="{{asset('images/home-main-logo-mobile.png')}}"/>
    </div>
    <div class="d-flex justify-content-center caption-container">
        <img class="home-caption-logo general" src="{{asset('images/home-caption.png')}}"/>
        <img class="home-caption-logo potrait" src="{{asset('images/home-caption-mobile.png')}}"/>
    </div>

    <div class="flex moment-section">
    @for ($i=0;$i<count($userOrder);$i++)

        @if($i==0 || $i==5)
            <div class="d-flex justify-content-center">    
        @endif
        
        <div class="p-0 image-content mw-100 image-content-{{$i}}"  onclick="showInfoAtDetail(this,{{$i}})">
            @if($userOrder[$i]!=false)
                @if($userOrder[$i]->is_valid==1)
                    <img class="checklist" src="{{asset('images/checklist.png')}}"/>
                @else
                    <div class="checklist fa fa-pause"></div>
                @endif
           {{--  @elseif($i+1 == $currentOrder)
            @elseif((($i+1)%2 == 0))
                <img class="img-bg overlay" src="{{asset('images/overlay-'.($i+1).'.png')}}"/>
            @else
                <img class="img-bg overlay" src="{{asset('images/overlay-odd.png')}}"/> --}}
            @endif
            <img class="img-bg" id="btn-img-{{$i}}" src="{{asset('images/thumb-'.($i+1).'.png')}}"/>
        </div>

        @if($i==4 || $i==9)
            </div>
        @endif
    @endfor
        <div class="d-flex justify-content-center">
            <div class="submit-area">
                <div class="submit-content">
                    <div class="left-section">

                        <img class="title-submit-area" src="{{asset('images/submit-caption.png')}}"/>
                        {{-- @if($canUpload) --}}
                        <div class="days-info instruction-text" id="order">
                            Moment ke {{$currentOrder}}
                        </div>
                        <div class="days-caption instruction-text" id="theme-text">
                            {{$order->theme}}
                        </div>
                         @if(isset($reason))
                            <div class="instruction-text">{{$reason}}</div>
                            @endif
                        {{-- @else
                            <div class="waiting instruction-text">
                                Foto moment ke-{{$currentOrder}} kamu masih dalam pemeriksaan, mohon tunggu
                            </div>
                        @endif --}}
                    </div>

                    <div class="right-section">
                        @if($canUpload)
                            <button class="upload-button btn" data-toggle="modal" data-target="#exampleModalLong">
                                <div class="upload-copytext">
                                    <span class="text-plus">+</span>
                                    <br>
                                    Upload
                                    <br>
                                    Challenge ke-<span id="challenge-number">{{$currentOrder}}</span>
                                </div>
                            </button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade modal-popup" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="d-flex justify-content-center">
            <img class="modal-title" src="{{asset('images/modal-title.png')}}"/>
        </div>
        
        <div class="modal-caption" id="modal-caption-form">{{$order->theme}}</div>
        
        <div class="d-flex justify-content-center">
            <img id="preview" class="preview-image" src="{{asset('images/preview.png')}}"/>
        </div>

        <form method="post" enctype="multipart/form-data" action="{{route('best-moment.store')}}">
            @csrf
            <input name="order" id="order-index-form" type="hidden" value="{{$currentOrder}}"/>
            <input name="image" onchange="readURL(this)" type="file" class="custom-file-input" accept="image/x-png,image/jpeg" required/>
            <div class="d-flex justify-content-center">
                <input type="submit" class="upload-image-button" value="UPLOAD">
            </div>
        </form>
      </div>
    </div>
  </div>
</div>


@endsection

@section('script')
<script type="text/javascript">
var Orders = [
    "{{$allOrder[0]->theme}}",
    "{{$allOrder[1]->theme}}",
    "{{$allOrder[2]->theme}}",
    "{{$allOrder[3]->theme}}",
    "{{$allOrder[4]->theme}}",
    "{{$allOrder[5]->theme}}",
    "{{$allOrder[6]->theme}}",
    "{{$allOrder[7]->theme}}",
    "{{$allOrder[8]->theme}}",
    "{{$allOrder[9]->theme}}"
]
var currentSelected = null;
var defaultOrderText = $("#order").html();
var defaultThemeText =  $("#theme-text").html();
var currentOrderSelected = 99;
var currentIndex = ({{$currentOrder}}-1);

function showInfoAtDetail(obj,val)
{
    if(currentSelected!=null)
    {
        if(currentOrderSelected != val){
            currentOrderSelected = val;
            currentSelected.removeClass('selected');
            currentSelected = $("#btn-img-"+val);
            currentSelected.addClass('selected');
        }else{
            currentOrderSelected = 99;
            currentSelected.removeClass('selected');
            val = currentIndex
        }   
    }else{
        currentOrderSelected = val;
        currentSelected = $("#btn-img-"+val);
        currentSelected.addClass('selected');
    }

    setChallenge(val);
}

function setChallenge(val)
{
    $("#modal-caption-form").html(Orders[val]);
    $("#order-index-form").attr("value",val+1);
    $("#challenge-number").html(val+1);
    $("#order").html("Moment ke "+(val+1));
    $("#theme-text").html(Orders[val]);
}
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#preview')
                .attr('src', e.target.result).
                width('95%');
        };

        reader.readAsDataURL(input.files[0]);
    }
}
</script>
@endsection

@section('style')
<style>
    .video-status{
        color:white;
    }
    .share-area{
        position: absolute;
        left: 0;
        right: 0;
        bottom: -80px;
        text-align: center;
    }
    .share-image{
        width: 100px;
    }
     .btn-whatsapp{
        color:white;
        background-color: #2CC64E;
    }
    .btn-whatsapp:hover{
        color:white;
        background-color: #1BB53D;
    }
.selamat-img{
    width: 60vw;
    height: 60vh;
    position: absolute;
    margin:auto;
    top: 40%;
    transform: translateY(-50%);
    background-size: contain;
    background-position: center;
    background-repeat: no-repeat;
}
.overlay-finished{
    position: absolute;
    width: 100vw;
    height: 100vh;
    background-color: rgba(0,0,0,0.7);
    z-index: 100;
}
.selected{
    border: solid 2px rgba(255,255,255,0.8);
    border-radius: 7px;
}
.overlay{
    position: absolute;
    top:0;
    left:2.5%;
    right:2.5%;
    width: 95%;
}
.home-section{
    margin-top: 60px;
}
.caption-container{
    margin-top: 0px;
}
.home-main-logo{
    height: 10vh;
}
.home-caption-logo{
    height: 3vh;
}
.image-content{
    cursor:pointer;
    position: relative;
    text-align: center;
    width: 12%;
    /*max-width: 125px !important;*/
    margin-top: 10px;
    margin-left: 0.5%;
    margin-right: 0.5%;
    transform:scale(1);
    transition: 0.1s;
}
.modal-dialog{
    margin-top: 0px !important;
}
.image-content:active{
    transform:scale(0.9);
    transition: 0.1s;
}
.img-bg{
    width: 95%;
    height: auto;
}
.moment-section{
    margin-top: 30px;
}
.submit-area{
    padding-left: 0.5%;
    padding-right: 0.5%;
    width: 65%;
    max-width: 680px;
    height: 20vh;
    margin-top: 1%
}
.submit-content{
    width: 100%;
    height: 100%;
    background-color: #7DB538;
    border-radius: 6px;
    position: relative;
}
.left-section{
    position: absolute;
    left: 10px;
    top: 50%;
    color: white;
    width: 70%;
    transform: translatey(-50%);
}
.right-section{
    position: absolute;
    right: 2.5%;
    height: 90%;
    top:50%;
    width: 20%;
    transform: translateY(-50%);
    /*background-color: black;*/
}
.instruction-text{
    width:100%;
    margin-left: 10px;
}
.upload-button{
    right: 0;
    width: fit-content;
    height: 100%;
    background-color: white;
}
.button-image{
    height: 100%;
}
.checklist{
    color:white;
    position: absolute;
    right: 10px;
    top: 5px;
    width: 15%;
    height: auto;
}
.modal-body{
    background-color:transparent;
    /*background-image: url('');*/
    /*background-size: 100% auto;*/
}   
.modal-content{
    background-color: #7CB537;
    border-style: none;
}
.image-modal-bg{
    width: 100%;
    height: auto;
}
.modal-title{
    width: 80%;
}
.modal-caption{
    word-wrap: break-word;
    width: 95%;
    text-align: center;
    background-color: #EBEBEB;
    color:#3F5C1C;
    padding:5px;
    border-radius: 7px;
    margin:auto;
}
.preview-image{
    width: 95%;
    margin:auto;
    margin-top: 10px;
}
.modal-dialog{
    max-width: 600px !important;
}
.upload-image-button{
    /*background-image:url('{{asset('images/upload.png')}}');*/
    /*background-size: 100% 100%;*/
    background-color:white;
    background-position: center center;
    background-repeat: no-repeat;
    width: 95%;
    height: 100px;
    margin-top: 10px;
    font-size: 4vw;
    font-family: 'cookies';
    color:#7CB537 !important;
}
.custom-file-input::-webkit-file-upload-button {
  visibility: hidden;
}
.custom-file-input::before {
  content: 'Select some files';
  display: inline-block;
  background: linear-gradient(top, #f9f9f9, #e3e3e3);
  border: 1px solid #999;
  border-radius: 3px;
  padding: 5px 8px;
  outline: none;
  white-space: nowrap;
  -webkit-user-select: none;
  cursor: pointer;
  text-shadow: 1px 1px #fff;
  font-weight: 700;
  font-size: 10pt;
}
.custom-file-input:hover::before {
  border-color: black;
}
.custom-file-input:active::before {
  background: -webkit-linear-gradient(top, #e3e3e3, #f9f9f9);
}
.custom-file-input{
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    height: 40%;
}
.upload-copytext{
    text-align: center;
    font-family: 'cookies';
    color:#3F5C1C;
    width:100%;
    font-size: 2.5vh;
}
.text-plus{
    font-size: 2vw;
    line-height: 1.2vw;
}
.general{
        display: block;
}
.moble{
        display: none;
}
.title-submit-area{
        width: 30vw;
        max-width: 200px;
}
.instruction-text{
        font-size:1vw;
    }

@media (orientation: portrait) {
    .upload-copytext{
       font-size: 1.2vw;
    }
    .general{
        display: none;
    }
    .moble{
        display: block;
    }
    .home-caption-logo{
        height: auto;
        width: 60vw;
    }
    .home-section{
        margin-top: 50px;
        width: 90vw !important;
    }
    .image-content{
        width: 100%;
    }
    .moment-section{
        margin-top: 10px;
    }
    .submit-area{
        width: 100%;
        height: 15vh;
    }
    .title-submit-area{
        width: 30vw;
        max-width: 200px;
    }
    .instruction-text{
        font-size:2.5vw;
    }
    .upload-copytext{
        font-size: 2vw;
    }
    .text-plus{
        font-size: 4vw;
        line-height: 1.2vw;
    }
    .general{
        display: none;
    }
    .potrait{
        display: table;
    }
    .maskot{
        width: 40vw !important;
        height: 28vh !important;
    }
    .maskot-left{
        background-position: right bottom !important;
    }
    .maskot-right{
        background-position: left bottom !important;
    }
}
</style>
@endsection

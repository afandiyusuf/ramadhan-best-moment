@extends('layouts.app')

@section('content')


<div class="container">    
    <div class="d-flex justify-content-center brand-logo">
        <img class="brand-logo-image margin-auto" src="{{asset('images/landing-brand.png')}}"/>
    </div>
    <div class="d-flex justify-content-center main-logo-container">
        <img class="landing-main-logo" src="{{asset('images/landing-main-logo.png')}}"/>
    </div>
    <div class="d-flex justify-content-center caption-logo-container">
        <img class="landing-caption-logo general" src="{{asset('images/home-caption.png')}}"/>
        <img class="landing-caption-logo potrait" src="{{asset('images/home-caption-mobile.png')}}"/>
    </div>
    <div class="d-flex justify-content-center caption-logo-container">
        <img class="hashtag-logo" src="{{asset('images/hashtag.png')}}"/>
    </div>
    <div class="justify-content-center social-btn-container">
        <div class="d-flex justify-content-center login-register-container">
            <a class="btn btn-primary btn-login btn-access" href="{{route('login')}}">
                    {{ __('Login') }}
            </a>
            <a class="btn btn-primary btn-register btn-access "  href="{{route('register')}}">
                    {{ __('Register') }}
            </a>
        </div>
       {{--  <a class="btn btn-block btn-social btn-twitter white" 
            href="{{route('login.provider',['provider'=>'twitter'])}}">
            <span class="fa fa-twitter white"></span> {{__('Login with twitter')}}
        </a> --}}
        <a class="btn btn-block btn-social btn-google white"
            href="{{route('login.provider',['provider'=>'google'])}}" >
            <span class="fa fa-google white"></span> {{__('Login with google')}}
        </a>
        {{-- <a class="btn btn-block btn-social btn-facebook white" 
            href="{{route('login.provider',['provider'=>'facebook'])}}">
            <span class="fa fa-facebook white"></span> {{__('Login with facebook')}}
        </a> --}}
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    
</script>
@endsection

@section('style')
<style>
    @guest
    @else
    html body{
        height: 100%;
        overflow: hidden;
    }
    @endguest

.section-card{
    padding-left: 5px;
    padding-right: 5px;
}
.line-it-button{
    vertical-align: middle !important;
}
#card-section::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #1888B8;
}

#card-section::-webkit-scrollbar
{
    width: 6px;
    background-color: #F5F5F5;
}

#card-section::-webkit-scrollbar-thumb
{
    background-color: white;
}
.like-area{
    position: absolute;
    left: 0;
    top: 50%;
    transform: translateY(-50%);
}
.share-area{
    position: absolute;
    right: 0;
    top: 50%;
    transform: translateY(-50%);
}


    .btn-whatsapp{
        background-color: #2CC64E;
    }
    
    .card-section{
        margin-top: 37vh;
        overflow-y: scroll;
        height: 65vh;
    }
    .fixed-content{
        position: fixed;
        z-index: 1000;
    }
    .brand-logo{
        margin-top: 10vh;
    }
    .share-content{
        text-align: right;
    }
    .card-like{
        margin-top: 10px;
        height: 60px;
    }
    .user-section{
        margin-top: 10px;
    }
    .label-poster{
        color:#7DB538;
    }
    .card-container{
        margin-top: 5px;
        background-color: #7DB538;
        padding:10px;
    }
    .card-content{
        background-color: #EBEBE9;
    }
    .card-image-content{
        margin-top: 10px;
    }
    .image-user{
        height: 300px;
    }
    .label{
        color:white !important;
    }
    .white{
        color:white !important;
    }
    .main-logo-container{
        margin-top: 5vh;
    }
    .landing-main-logo{
        height: 30vh;
    }
    .caption-logo-container{
        margin-top: 2vh;
    }
    .landing-caption-logo{
        /*height: 5vh;*/
        width: 60vw;
        max-width: 700px;
        height: auto;
    }
    .social-btn-container{
        margin-top: 2vh;
    }
    .btn-social{

        width: 15vw;
        min-width: 250px;
        color: white !important;
        margin:auto;
    }
    .btn-social-icon{

    }
    .btn-login{
        width: 7.2vw;
        min-width: 200px;
        margin-right: 0.3vw;
    }
    .btn-register{
        width: 7.2vw;
        min-width: 200px;
        margin-left: 0.3vw;
    }

    .hashtag-logo{
        width: 30vw;
    }
    .login-register-container{
        margin-bottom: 2vh;
        color:#91C846 !important;
    }

   a {
        color:#91C846 !important;
    }
    .btn-access:hover{
        background-color: #91C846;
        color: white !important;
    }
    .btn-access:active{
        background-color: #91C846 !important;
        color: white !important;
    }
    .brand-logo-image{
        height: 10vh;
    }
    @media (orientation: portrait) {
        .brand-logo-image{
            margin-top: -2vh;
            width: 70vw;
            height: auto;
        }
        .landing-main-logo{
            width: 85vw;
            height: auto;
        }
        .landing-caption-logo{
            width: 60vw;
            height: auto;
        }
        .btn-access{
            display: table !important;
            margin:auto !important;
            margin-bottom: 10px !important;
        }
        .login-register-container{
            display: block !important;
            /*margin:auto;*/
            /*align-content: center;*/
        }
        .main-logo-container{
            margin-top: 2vh;
        }
       .maskot{
            /*height: 20vh !important;*/
                width: 30vw !important;
                height: 20vh !important ;
                background-size: contain !important;
                bottom: 0;  
        }
        .maskot-left{
                background-position: bottom left !important;
                left:10px !important;
            }
            .maskot-right{
                background-position: bottom right !important;
                right:10px !important;
            }
        @media (max-height: 600px)
        {
            
            
            .card{
                width: 80vw;
                margin-bottom: 100px;
            }
        }
    }
</style>
@endsection